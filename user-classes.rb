
# esse arquivo delimita todas as classes de usuario

require 'date'

module ClassesUsuario

    
    class Usuario
        attr_accessor :nome, :foto

        def initialize(nome:, foto:, matricula:, cpf:, email:, senha:, nascimento:, endereco:)
            @nome = nome
            @foto = foto
            @matricula = matricula
            @cpf = cpf
            @email = email
            @senha = senha
            @nascimento = Date.strptime(nascimento, '%d-%m-%Y')
            @endereco = endereco
            @logado = false
        end

        def logado()
            @logado
        end

        def idade()
            #Retorna a idade do usuário, levando em conta a data
            now = Time.now.utc.to_date
            return now.year - @nascimento.year - ((now.month > @nascimento.month || (now.month == @nascimento.month && now.day >= @nascimento.day)) ? 0 : 1)
        end

        def logar(email, senha)
            #Modifica a variável logado para true, caso a senha esteja certa
            @logado = email.downcase == @email.downcase && senha == @senha
        end

        def deslogar()
            #Modifica a variável logado para false.
            @logado = false
        end
    end

    class Aluno < Usuario

        def initialize(nome:, foto:, matricula:, cpf:, email:, senha:, nascimento:, endereco:, curso:, periodo:)
            super(nome: nome, foto: foto, matricula: matricula, cpf: cpf, email: email, senha: senha, nascimento: nascimento, endereco: endereco)
            @periodo = periodo
            @curso = curso
            @turmas = []
        end

        def inscrever(turma)
            #Adiciona a turma do aluno no array de turmas.
            unless @logado
                raise "NotLoggedException"
                return
            end

            if turma.adicionar_aluno(self)
                return
            end

            @turmas.push(turma)
        end

    end

    class Professor < Usuario

        def initialize(nome:, foto:, matricula:, cpf:, email:, senha:, nascimento:, endereco:, salario:, materias:)
            super(nome: nome, foto: foto, matricula: matricula, cpf: cpf, email: email, senha: senha, nascimento: nascimento, endereco: endereco)
            @salario = salario
            @materias = materias
            @turmas = []
        end

        def adicionar_materia(materia)
            #Adiciona nome_materia ao array de materias.
            materias.push(materia)
        end

        def adicionar_turma(turma)
            #Adiciona turma ao array de materias.
            unless @logado
                raise "NotLoggedException"
                return
            end

            if turma.adicionar_professor(self)
                return
            end

            @turmas.push(turma)
        end

    end
end