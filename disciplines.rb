#esse arquivo delimita todas as classes das disciplinas


module ClassesDisciplina
    class Materia

        attr_accessor :nome, :ementa, :horas_totais, :requisitos, :materias_similares, :codigo_materia

        def initialize(nome:, ementa:, horas_totais:, requisitos:, materias_similares:, codigo_materia:)
            @nome = nome
            @ementa = ementa
            @horas_totais = horas_totais
            @requisitos = requisitos
            @materias_similares = materias_similares
            @codigo_materia = codigo_materia
            @professores = []
        end

        def adicionar_professor(nome_professor)
            # Adiciona professor ao array de professores.
            @professores.push(nome_professor)
        end
    end

    class Turma
        attr_accessor :alunos_inscritos, :max_alunos, :turma_codigo, :ano, :periodo, :cronograma_semanal, :sala, :bloco, :materia_base

        def initialize(max_alunos:, turma_codigo:, ano:, periodo:, cronograma_semanal:, sala:, bloco:, materia_base:)
            @professor = -1
            @alunos_inscritos = []
            @max_alunos = max_alunos
            @turma_codigo = turma_codigo
            @ano = ano
            @periodo = periodo
            @cronograma_semanal = cronograma_semanal
            @sala = sala
            @bloco = bloco
            @materia_base = materia_base
            @inscricoes_abertas = false
        end

        def inscricoes_abertas
            #get de inscricoes abertas
            @inscricoes_abertas
        end

        def abrir_inscricao()
            #modifica a variável inscrição_aberta para true
            @inscricoes_abertas = true
        end

        def fechar_inscricao()
            #modifica a variável inscrição_aberta para false
            @inscricoes_abertas = false
        end

        def adicionar_professor(professor)
            #adiciona professor ao campo professor
            if @professor != -1 || !@inscricoes_abertas
                raise "FullException"
                return false
            end

            @professor = professor
            return true
        end

        def adicionar_aluno(aluno)
            #adiciona um aluno no array de aluno caso haja vagas
            if @alunos_inscritos.length >= @max_alunos || !@inscricoes_abertas
                raise "FullException"
                return false
            end
            @alunos_inscritos.append(aluno)
            return true
        end

    end
end